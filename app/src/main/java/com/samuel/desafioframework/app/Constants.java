package com.samuel.desafioframework.app;

public class Constants {

    public class SharedPreferences {
        public static final String PATH = "desafio_path";
    }

    public class Service {

        public static final String GET_POSTS = "posts";
        public static final String GET_ALBUMS = "albums";
        public static final String GET_TODOS = "todos";

    }

}
