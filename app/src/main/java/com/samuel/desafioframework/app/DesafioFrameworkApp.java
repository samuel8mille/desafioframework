package com.samuel.desafioframework.app;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DesafioFrameworkApp extends Application {

    private static DesafioFrameworkApp instance;

    public static DesafioFrameworkApp getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        init();
    }

    private void init() {
        initRealm();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration myConfig = new RealmConfiguration.Builder()
                .name("desafioframework.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(myConfig);
    }
}
