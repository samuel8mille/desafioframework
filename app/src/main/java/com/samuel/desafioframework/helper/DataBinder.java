package com.samuel.desafioframework.helper;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.genius.groupie.GroupAdapter;

public class DataBinder {

    @BindingAdapter(value = "android:visibility")
    public static void setVisible(View view, boolean isShow) {
        view.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value = "adapterNested")
    public static void setAdapterNested(RecyclerView recyclerView, GroupAdapter groupAdapter) {
        recyclerView.setAdapter(groupAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }
}
