package com.samuel.desafioframework.helper;

import android.content.Context;
import android.text.TextUtils;

import com.afollestad.materialdialogs.MaterialDialog;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.service.model.ToDoModel;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;

public class Utils {

    public static boolean isThrowableInternetOff(Throwable t) {
        return t instanceof UnknownHostException || t instanceof IOException || t instanceof SocketTimeoutException;
    }

    private static String getMensagemThrowable(Context context, Throwable t) {
        if (t instanceof HttpException) {
            try {
                return parserError(context, ((HttpException) t).response().errorBody().string());
            } catch (Exception e) {
                return context.getString(R.string.msg_error_generic);
            }
        } else if (isThrowableInternetOff(t)) {
            return context.getString(R.string.msg_erro_sem_internet);
        }

        return context.getString(R.string.msg_error_generic);
    }

    private static String parserError(Context context, String error) {
        if (TextUtils.isEmpty(error)) {
            return context.getString(R.string.msg_error_generic);
        }
        return context.getString(R.string.msg_error_generic);
    }

    public static void showDialogError(Context context, Throwable t, MaterialDialog.SingleButtonCallback callback) {
        try {
            String msgError = getMensagemThrowable(context, t);
            new MaterialDialog.Builder(context)
                    .content(msgError)
                    .cancelable(false)
                    .positiveText(android.R.string.ok)
                    .onPositive(callback)
                    .show();
        } catch (Exception ex) {

        }
    }

    public static Observable<List<AlbumModel>> getListToAlbums(List<AlbumModel> albums) {
        List<AlbumModel> list = new ArrayList<>();
        Iterator iterator = albums.iterator();
        try {
            while (iterator.hasNext()) {
                AlbumModel album = (AlbumModel) iterator.next();
                list.add(album);
            }
            return Observable.just(list);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }

    public static Observable<List<PostModel>> getListToPosts(List<PostModel> posts) {
        List<PostModel> list = new ArrayList<>();
        Iterator iterator = posts.iterator();
        try {
            while (iterator.hasNext()) {
                PostModel post = (PostModel) iterator.next();
                list.add(post);
            }
            return Observable.just(list);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }

    public static Observable<List<ToDoModel>> getListToToDos(List<ToDoModel> toDos) {
        List<ToDoModel> list = new ArrayList<>();
        Iterator iterator = toDos.iterator();
        try {
            while (iterator.hasNext()) {
                ToDoModel toDo = (ToDoModel) iterator.next();
                list.add(toDo);
            }
            return Observable.just(list);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }
}
