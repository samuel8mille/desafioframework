package com.samuel.desafioframework.helper.bd;

import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.service.model.ToDoModel;

import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import rx.Observable;

public class BDHelper {

    public static Observable<Void> saveAlbums(List<AlbumModel> albumsList) {
        Iterator iterator = albumsList.iterator();
        Realm realm = Realm.getDefaultInstance();
        try {
            while (iterator.hasNext()) {
                realm.beginTransaction();
                AlbumModel album = (AlbumModel) iterator.next();
                realm.insertOrUpdate(album);
                realm.commitTransaction();
            }
            return Observable.just(null);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }

    public static Observable<Void> savePosts(List<PostModel> postList) {
        Iterator iterator = postList.iterator();
        Realm realm = Realm.getDefaultInstance();
        try {
            while (iterator.hasNext()) {
                realm.beginTransaction();
                PostModel post = (PostModel) iterator.next();
                realm.insertOrUpdate(post);
                realm.commitTransaction();
            }
            return Observable.just(null);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }

    public static Observable<Void> saveToDos(List<ToDoModel> toDoList) {
        Iterator iterator = toDoList.iterator();
        Realm realm = Realm.getDefaultInstance();
        try {
            while (iterator.hasNext()) {
                realm.beginTransaction();
                ToDoModel toDo = (ToDoModel) iterator.next();
                realm.insertOrUpdate(toDo);
                realm.commitTransaction();
            }
            return Observable.just(null);
        } catch (Exception ex) {
            return Observable.error(ex);
        }
    }

    public static Observable<List<AlbumModel>> getAlbums() {
        Realm realm = Realm.getDefaultInstance();
        return Observable.just(realm.where(AlbumModel.class).findAll());
    }

    public static Observable<List<PostModel>> getPosts() {
        Realm realm = Realm.getDefaultInstance();
        return Observable.just(realm.where(PostModel.class).findAll());
    }

    public static Observable<List<ToDoModel>> getToDos() {
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(ToDoModel.class).findAll().isEmpty())
            return Observable.error(new Throwable("Empty db"));
        return Observable.just(realm.where(ToDoModel.class).findAll());
    }
}
