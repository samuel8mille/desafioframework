package com.samuel.desafioframework.ui.adapter.todos;

import com.genius.groupie.Item;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.databinding.ItemListToDoBinding;
import com.samuel.desafioframework.service.model.ToDoModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.todos.ItemListToDosViewModel;

import java.util.List;

public class ToDosListAdapter extends Item<ItemListToDoBinding> {

    private CallbackBasicViewModel callback;
    private List<ToDoModel> list;

    public ToDosListAdapter(List<ToDoModel> list, CallbackBasicViewModel callback) {
        this.callback = callback;
        this.list = list;
    }

    @Override
    public int getLayout() {
        return R.layout.item_list_to_do;
    }

    @Override
    public void bind(ItemListToDoBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemListToDosViewModel(list.get(position), callback));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
