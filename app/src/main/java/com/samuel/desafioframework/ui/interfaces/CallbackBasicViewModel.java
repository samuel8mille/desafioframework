package com.samuel.desafioframework.ui.interfaces;

import com.afollestad.materialdialogs.MaterialDialog;

public interface CallbackBasicViewModel {

    void showDialogProgress();

    void hideDialogProgress();

    void showError(Throwable t, MaterialDialog.SingleButtonCallback callback);

}
