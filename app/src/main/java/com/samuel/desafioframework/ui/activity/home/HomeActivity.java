package com.samuel.desafioframework.ui.activity.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.android.databinding.library.baseAdapters.BR;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.databinding.ActivityHomeBinding;
import com.samuel.desafioframework.ui.activity.base.BaseActivity;
import com.samuel.desafioframework.viewmodel.albums.AlbumsViewModel;
import com.samuel.desafioframework.viewmodel.posts.PostsViewModel;
import com.samuel.desafioframework.viewmodel.todos.ToDosViewModel;

import io.github.rakshakhegde.lastpageradapter.LastPagerAdapter;
import it.sephiroth.android.library.bottomnavigation.BottomNavigation;

public class HomeActivity extends BaseActivity {

    ActivityHomeBinding mBinding;
    PostsViewModel postsViewModel;
    AlbumsViewModel albumsViewModel;
    ToDosViewModel toDoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setToolbar();
        initViewModels();
        initViewPagerListener();
        initAdapterViewPager();
        initListenersBottomBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBinding != null && mBinding.viewpager != null) {
            switch (mBinding.viewpager.getCurrentItem()) {
                case 0:
                    postsViewModel.resume();
                    break;
                case 1:
                    albumsViewModel.resume();
                    break;
                case 2:
                    toDoViewModel.resume();
                    break;
            }
        }
    }

    private void setToolbar() {
        setSupportActionBar(mBinding.includeToolbar.toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    private void initViewModels() {
        postsViewModel = new PostsViewModel(this);
        albumsViewModel = new AlbumsViewModel(this);
        toDoViewModel = new ToDosViewModel(this);
    }

    private void initAdapterViewPager() {
        new LastPagerAdapter(BR.viewModel)
                .add(R.layout.fragment_posts, "Posts", postsViewModel)
                .add(R.layout.fragment_albums, "Albums", albumsViewModel)
                .add(R.layout.fragment_todos, "ToDos", toDoViewModel)
                .into(mBinding.viewpager);
    }

    private void initViewPagerListener() {
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        postsViewModel.resume();
                        break;
                    case 1:
                        albumsViewModel.resume();
                        break;
                    case 2:
                        toDoViewModel.resume();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private void initListenersBottomBar() {
        mBinding.BottomNavigation.setOnMenuItemClickListener(new BottomNavigation.OnMenuItemSelectionListener() {
            @Override
            public void onMenuItemSelect(int i, int i1, boolean b) {
                switch (i) {
                    case R.id.btn_posts:
                        mBinding.viewpager.setCurrentItem(0);
                        break;
                    case R.id.btn_albums:
                        mBinding.viewpager.setCurrentItem(1);
                        break;
                    case R.id.btn_todos:
                        mBinding.viewpager.setCurrentItem(2);
                        break;

                }
            }

            @Override
            public void onMenuItemReselect(int i, int i1, boolean b) {

            }
        });
    }
}
