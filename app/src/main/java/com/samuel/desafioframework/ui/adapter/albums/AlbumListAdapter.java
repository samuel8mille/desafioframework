package com.samuel.desafioframework.ui.adapter.albums;

import com.genius.groupie.Item;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.databinding.ItemListAlbumBinding;
import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.albums.ItemListAlbumsViewModel;

import java.util.List;

public class AlbumListAdapter extends Item<ItemListAlbumBinding> {

    private CallbackBasicViewModel callback;
    private List<AlbumModel> list;

    public AlbumListAdapter(List<AlbumModel> list, CallbackBasicViewModel callback) {
        this.callback = callback;
        this.list = list;
    }

    @Override
    public int getLayout() {
        return R.layout.item_list_album;
    }

    @Override
    public void bind(ItemListAlbumBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemListAlbumsViewModel(list.get(position), callback));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
