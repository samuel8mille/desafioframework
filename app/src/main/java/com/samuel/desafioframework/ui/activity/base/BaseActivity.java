package com.samuel.desafioframework.ui.activity.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.helper.Utils;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;

import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity implements CallbackBasicViewModel {

    protected Toolbar toolbar;
    private SweetAlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initProgressDialog() {
        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(ContextCompat.getColor(this, R.color.grape));
        progressDialog.setTitleText(getString(R.string.aguarde));
        progressDialog.setCancelable(false);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            assert getSupportActionBar() != null;
            toolbar.setNavigationIcon(null);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void showDialogProgress() {
        if (progressDialog == null) initProgressDialog();
        progressDialog.setTitleText(getString(R.string.aguarde));
        if (!progressDialog.isShowing()) progressDialog.show();
    }

    @Override
    public void hideDialogProgress() {
        if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
    }

    @Override
    public void showError(Throwable t, MaterialDialog.SingleButtonCallback callback) {
        Utils.showDialogError(this, t, callback);
    }

}
