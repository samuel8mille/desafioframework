package com.samuel.desafioframework.ui.adapter.posts;

import com.genius.groupie.Item;
import com.samuel.desafioframework.R;
import com.samuel.desafioframework.databinding.ItemListPostBinding;
import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.posts.ItemListPostsViewModel;

import java.util.List;

public class PostListAdapter extends Item<ItemListPostBinding> {

    private CallbackBasicViewModel callback;
    private List<PostModel> list;

    public PostListAdapter(List<PostModel> list, CallbackBasicViewModel callback) {
        this.callback = callback;
        this.list = list;
    }

    @Override
    public int getLayout() {
        return R.layout.item_list_post;
    }

    @Override
    public void bind(ItemListPostBinding viewBinding, int position) {
        viewBinding.setViewModel(new ItemListPostsViewModel(list.get(position), callback));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
