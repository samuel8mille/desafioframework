package com.samuel.desafioframework.viewmodel.todos;

import android.databinding.ObservableBoolean;

import com.genius.groupie.GroupAdapter;
import com.samuel.desafioframework.helper.RxJavaUtils;
import com.samuel.desafioframework.helper.Utils;
import com.samuel.desafioframework.helper.bd.BDHelper;
import com.samuel.desafioframework.service.model.ToDoModel;
import com.samuel.desafioframework.service.retrofit.RetrofitBase;
import com.samuel.desafioframework.ui.adapter.todos.ToDosListAdapter;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

import java.util.List;

import rx.Observable;

public class ToDosViewModel extends BaseViewModel {

    public final ObservableBoolean isShowProgressContainer = new ObservableBoolean(false);

    public final GroupAdapter groupAdapter = new GroupAdapter();

    public ToDosViewModel(CallbackBasicViewModel callback) {
        super(callback);
        verifyGetList(true);
    }

    private void verifyGetList(boolean isShowProgress) {
        getToDosList(isShowProgress);
    }

    private void getToDosList(boolean isShowProgress) {
        isShowProgressContainer.set(true);
        if (isShowProgress)
            showProgress();

        RetrofitBase.getInterfaceRetrofit().getToDos()
                .compose(RxJavaUtils.applySchedulers())
                .flatMap(toDoModels -> {
                    if (!toDoModels.isEmpty()) {
                        BDHelper.saveToDos(toDoModels);
                        return Utils.getListToToDos(toDoModels);
                    } else {
                        return Observable.error(new Throwable("Error"));
                    }
                })
                .subscribe(this::onSuccessToDos,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });

    }

    private void getListBD(boolean isShowProgress) {
        if (isShowProgress)
            showProgress();
        else
            isShowProgressContainer.set(true);

        BDHelper.getToDos()
                .subscribe(this::onSuccessToDos,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });
    }

    private void handleErrors(Throwable t) {
        isShowProgressContainer.set(false);
        hideProgress();
        if (Utils.isThrowableInternetOff(t)) {
            getListBD(false);
            return;
        }

        showError(t, (dialog, which) -> {
        });
    }

    private void onSuccessToDos(List<ToDoModel> todosResponse) {
        isShowProgressContainer.set(false);
        setList(todosResponse);
    }

    private void setList(List<ToDoModel> todosResponse) {
        groupAdapter.clear();
        groupAdapter.add(new ToDosListAdapter(todosResponse, callback));
    }
}
