package com.samuel.desafioframework.viewmodel.base;

import android.databinding.ObservableField;

import com.afollestad.materialdialogs.MaterialDialog;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;

public class BaseViewModel implements ViewModel {

    public final ObservableField<String> titleTextToolbar = new ObservableField<>("");
    protected CallbackBasicViewModel callback;

    public BaseViewModel(CallbackBasicViewModel callback) {
        this.callback = callback;
    }

    protected void showProgress() {
        if (callback != null) callback.showDialogProgress();
    }

    protected void hideProgress() {
        if (callback != null) callback.hideDialogProgress();
    }

    protected void showError(Throwable t, MaterialDialog.SingleButtonCallback callback2) {
        hideProgress();
        if (callback != null) callback.showError(t, callback2);
    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }
}
