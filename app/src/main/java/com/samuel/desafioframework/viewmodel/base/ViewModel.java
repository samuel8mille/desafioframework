package com.samuel.desafioframework.viewmodel.base;

public interface ViewModel {

    void resume();

    void destroy();
}
