package com.samuel.desafioframework.viewmodel.albums;

import android.databinding.ObservableField;

import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

public class ItemListAlbumsViewModel extends BaseViewModel {

    public final ObservableField<String> txtUserId = new ObservableField<>();
    public final ObservableField<String> txtId = new ObservableField<>();
    public final ObservableField<String> txtTitle = new ObservableField<>();

    public ItemListAlbumsViewModel(AlbumModel albumModel, CallbackBasicViewModel callback) {
        super(callback);

        try {
            txtUserId.set(albumModel.getUserId().toString());
            txtId.set(albumModel.getId().toString());
            txtTitle.set(albumModel.getTitle());
        } catch (Exception ex) {

        }

    }
}
