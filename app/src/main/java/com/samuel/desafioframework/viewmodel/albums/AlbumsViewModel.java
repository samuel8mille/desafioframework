package com.samuel.desafioframework.viewmodel.albums;

import android.databinding.ObservableBoolean;

import com.genius.groupie.GroupAdapter;
import com.samuel.desafioframework.helper.RxJavaUtils;
import com.samuel.desafioframework.helper.Utils;
import com.samuel.desafioframework.helper.bd.BDHelper;
import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.service.retrofit.RetrofitBase;
import com.samuel.desafioframework.ui.adapter.albums.AlbumListAdapter;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

import java.util.List;

import rx.Observable;

public class AlbumsViewModel extends BaseViewModel {

    public final ObservableBoolean isShowProgressContainer = new ObservableBoolean(false);

    public final GroupAdapter groupAdapter = new GroupAdapter();

    public AlbumsViewModel(CallbackBasicViewModel callback) {
        super(callback);
        verifyGetList(true);
    }

    private void verifyGetList(boolean isShowProgress) {
        getAlbumsList(isShowProgress);
    }

    private void getAlbumsList(boolean isShowProgress) {
        isShowProgressContainer.set(true);
        if (isShowProgress)
            showProgress();

        RetrofitBase.getInterfaceRetrofit().getAlbums()
                .compose(RxJavaUtils.applySchedulers())
                .flatMap(albumModels -> {
                    if (!albumModels.isEmpty()) {
                        BDHelper.saveAlbums(albumModels);
                        return Utils.getListToAlbums(albumModels);
                    } else {
                        return Observable.error(new Throwable("Error"));
                    }
                })
                .subscribe(this::onSuccessAlbums,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });
    }

    private void getListBD(boolean isShowProgress) {
        if (isShowProgress)
            showProgress();
        else
            isShowProgressContainer.set(true);

        BDHelper.getAlbums()
                .subscribe(this::onSuccessAlbums,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });
    }

    private void handleErrors(Throwable t) {
        isShowProgressContainer.set(false);
        hideProgress();
        if (Utils.isThrowableInternetOff(t)) {
            getListBD(false);
            return;
        }

        showError(t, (dialog, which) -> {
        });
    }

    private void onSuccessAlbums(List<AlbumModel> albumsResponse) {
        isShowProgressContainer.set(false);
        setList(albumsResponse);
    }

    private void setList(List<AlbumModel> postsResponse) {
        groupAdapter.clear();
        groupAdapter.add(new AlbumListAdapter(postsResponse, callback));
    }
}
