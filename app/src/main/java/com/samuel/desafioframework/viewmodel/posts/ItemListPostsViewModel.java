package com.samuel.desafioframework.viewmodel.posts;

import android.databinding.ObservableField;

import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

public class ItemListPostsViewModel extends BaseViewModel {

    public final ObservableField<String> txtUserId = new ObservableField<>();
    public final ObservableField<String> txtId = new ObservableField<>();
    public final ObservableField<String> txtTitle = new ObservableField<>();
    public final ObservableField<String> txtBody = new ObservableField<>();

    public ItemListPostsViewModel(PostModel postModel, CallbackBasicViewModel callback) {
        super(callback);
        try {
            txtUserId.set(postModel.getUserId().toString());
            txtId.set(postModel.getId().toString());
            txtTitle.set(postModel.getTitle());
            txtBody.set(postModel.getBody());
        } catch (Exception ex) {
        }
    }
}
