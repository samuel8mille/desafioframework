package com.samuel.desafioframework.viewmodel.posts;

import android.databinding.ObservableBoolean;

import com.genius.groupie.GroupAdapter;
import com.samuel.desafioframework.helper.RxJavaUtils;
import com.samuel.desafioframework.helper.Utils;
import com.samuel.desafioframework.helper.bd.BDHelper;
import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.service.retrofit.RetrofitBase;
import com.samuel.desafioframework.ui.adapter.posts.PostListAdapter;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

import java.util.List;

import rx.Observable;

public class PostsViewModel extends BaseViewModel {

    public final ObservableBoolean isShowProgressContainer = new ObservableBoolean(false);

    public final GroupAdapter groupAdapter = new GroupAdapter();

    public PostsViewModel(CallbackBasicViewModel callback) {
        super(callback);
        verifyGetList(true);
    }

    private void verifyGetList(boolean isShowProgress) {
        getPostsList(isShowProgress);
    }

    private void getPostsList(boolean isShowProgress) {
        isShowProgressContainer.set(true);
        if (isShowProgress)
            showProgress();

        RetrofitBase.getInterfaceRetrofit().getPosts()
                .compose(RxJavaUtils.applySchedulers())
                .flatMap(postModels -> {
                    if (!postModels.isEmpty()) {
                        BDHelper.savePosts(postModels);
                        return Utils.getListToPosts(postModels);
                    } else {
                        return Observable.error(new Throwable("Error"));
                    }
                })
                .subscribe(this::onSuccessPosts,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });
    }

    private void getListBD(boolean isShowProgress) {
        if (isShowProgress)
            showProgress();
        else
            isShowProgressContainer.set(true);

        BDHelper.getPosts()
                .subscribe(this::onSuccessPosts,
                        this::handleErrors,
                        () -> {
                            isShowProgressContainer.set(false);
                            hideProgress();
                        });
    }

    private void handleErrors(Throwable t) {
        isShowProgressContainer.set(false);
        hideProgress();
        if (Utils.isThrowableInternetOff(t)) {
            getListBD(false);
            return;
        }

        showError(t, (dialog, which) -> {
        });
    }

    private void onSuccessPosts(List<PostModel> postsResponse) {
        isShowProgressContainer.set(false);
        setList(postsResponse);
    }

    private void setList(List<PostModel> postsResponse) {
        groupAdapter.clear();
        groupAdapter.add(new PostListAdapter(postsResponse, callback));
    }
}
