package com.samuel.desafioframework.viewmodel.todos;

import android.databinding.ObservableField;

import com.samuel.desafioframework.service.model.ToDoModel;
import com.samuel.desafioframework.ui.interfaces.CallbackBasicViewModel;
import com.samuel.desafioframework.viewmodel.base.BaseViewModel;

public class ItemListToDosViewModel extends BaseViewModel {

    public final ObservableField<String> txtUserId = new ObservableField<>();
    public final ObservableField<String> txtId = new ObservableField<>();
    public final ObservableField<String> txtTitle = new ObservableField<>();
    public final ObservableField<String> completed = new ObservableField<>();

    public ItemListToDosViewModel(ToDoModel toDoModel, CallbackBasicViewModel callback) {
        super(callback);

        try {
            txtUserId.set(toDoModel.getUserId().toString());
            txtId.set(toDoModel.getId().toString());
            txtTitle.set(toDoModel.getTitle());
            completed.set(toDoModel.getCompleted().toString());
        } catch (Exception e) {
        }
    }
}
