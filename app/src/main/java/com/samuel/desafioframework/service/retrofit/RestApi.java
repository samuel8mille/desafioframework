package com.samuel.desafioframework.service.retrofit;

import com.samuel.desafioframework.app.Constants;
import com.samuel.desafioframework.service.model.AlbumModel;
import com.samuel.desafioframework.service.model.PostModel;
import com.samuel.desafioframework.service.model.ToDoModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface RestApi {

    @GET(Constants.Service.GET_POSTS)
    Observable<List<PostModel>> getPosts();

    @GET(Constants.Service.GET_ALBUMS)
    Observable<List<AlbumModel>> getAlbums();

    @GET(Constants.Service.GET_TODOS)
    Observable<List<ToDoModel>> getToDos();
}
